package com.cdg.polymeric.core.ext.excutor;

public class ExcutorResultMatcher {

    /**
     * 基础类型匹配包装类型
     * @param className
     * @return
     */
    public static String typeMatch(String className){
        switch (className){
            case "int": return "java.lang.Integer";
            case "long": return "java.lang.Long";
            case "byte": return "java.lang.Byte";
            case "boolean": return "java.lang.Boolean";
            case "char": return "java.lang.Character";
            case "short": return "java.lang.Short";
            case "double": return "java.lang.Double";
            case "float": return "java.lang.Float";

        }

        return className;
    }
}
