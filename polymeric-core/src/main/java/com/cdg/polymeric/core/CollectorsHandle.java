package com.cdg.polymeric.core;

import com.cdg.polymeric.Collectors;
import com.cdg.polymeric.Environment;
import com.cdg.polymeric.config.AnnotationSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import java.util.concurrent.CopyOnWriteArrayList;

public class CollectorsHandle {
    private static final Logger logger = LoggerFactory.getLogger(CollectorsHandle.class);
    private CopyOnWriteArrayList<AnnotationSetting> annotationSettings = new CopyOnWriteArrayList<>();
    private Environment environment;

    public  CopyOnWriteArrayList<? extends AnnotationSetting> getAnnotationSettings() {
        return annotationSettings;
    }

    public CollectorsHandle collectorProcess(List<Collectors> collectors){
        collectors.stream().forEach(collector->{
            if (collector == null){
                logger.info("collector is not be null");
            }else{
                 collector.collect(annotationSettings);
            }
        });

        return this;
    }
}
