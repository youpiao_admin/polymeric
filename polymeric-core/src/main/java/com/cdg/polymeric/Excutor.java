package com.cdg.polymeric;

/**
 * 执行远程数据源聚合接口
 * @param <T>
 */
public interface Excutor<T> {
    public T join();
}
