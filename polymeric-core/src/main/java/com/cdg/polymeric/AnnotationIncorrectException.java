package com.cdg.polymeric;

public class AnnotationIncorrectException extends RuntimeException{

    public AnnotationIncorrectException(String message){
        super(message);
    }

    public AnnotationIncorrectException(String message,Throwable e){
        super(message,e);
    }
}
