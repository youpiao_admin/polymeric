package com.cdg.polymeric;

import com.cdg.polymeric.config.AnnotationSetting;
import com.google.common.cache.LoadingCache;
import org.apache.ibatis.reflection.Reflector;


public interface Environment<T> {

   /**
    * 获取运程查询执行器
    * @return
    */
   public Excutor<T> getExcutor();

   /**
    * 获取查修配置
    * @return
    */
   public AnnotationSetting getAnnotationSetting();

   /**
    * 获取 ibatis Reflector
    * @return
    */
   public Reflector getReflector();

   /**
    *  获取缓存
    * @return
    */
   public LoadingCache<String, Object> loadCache();


}
