package com.cdg.polymeric.annonation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 支持多数据源聚合
 * @author wyp
 * @date 2022-11-21
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE})
public @interface PolymericConfig {

    Polymeric[] sources();
}
