package com.cdg.polymeric.annonation;



import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

/**
 * @author wyp
 * @date 2020-10-5
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.FIELD})
public @interface PolymericFiled {

    /**
     * 查询值
     * @return
     */
    String key() default "";


    /**
     * 目标类
     * @return
     */
    Class<?> feign() ;

    /**
     * 调用方法
     * @return
     */
    String method() default "";

    /**
     * 方法参数
     * @return
     */
    Class<?>[] args() default {List.class};


    /**
     * 是否启用缓存
     * @return
     */
    boolean enableCache() default false;



}
