package com.cdg.polymeric.annonation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

/**
 * @author wyp
 * @date 2021-07-09
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE})
public @interface Polymeric {

    /**
     * 查询值
     * @return
     */
    String key() default "";



    /**
     * 目标类
     * @return
     */
    Class<?> feign() ;

    /**
     * 调用方法
     * @return
     */
    String method() default "";

    /**
     * 方法参数
     * @return
     */
    Class<?>[] args() default {List.class};

    /**
     * 是否开启批量查询
     * @return
     */
    boolean batch() default false;



}
