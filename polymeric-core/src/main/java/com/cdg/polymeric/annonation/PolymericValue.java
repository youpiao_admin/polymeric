package com.cdg.polymeric.annonation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wyp
 * @version 2021-07-15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.FIELD})
public @interface PolymericValue {
    String name() default "";
    boolean enableCache() default false;
}
