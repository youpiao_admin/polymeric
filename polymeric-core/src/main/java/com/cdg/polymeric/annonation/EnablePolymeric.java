package com.cdg.polymeric.annonation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author wyp
 * @date 2020-10-5
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
//@Import({PolymericAutoConfiguration.class})
public @interface EnablePolymeric {
}
