package com.cdg.polymeric;

import com.cdg.polymeric.config.AnnotationSetting;
import com.cdg.polymeric.config.ClassAnnotationProperties;
import com.cdg.polymeric.config.ClassAnnotationSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AnnotationSettingBuilder {
    private final static Logger logger = LoggerFactory.getLogger(AnnotationSettingBuilder.class);

    private String key;

    private Class<?> feign;

    private String method;

    private Class<?>[] args;

    private boolean batch;

    private String[] fieldQuerySetting;

    private boolean enableCache;

    private Class<?> resultType;

    public AnnotationSettingBuilder key(String key){
        this.key = key;
        return this;
    }

    public AnnotationSettingBuilder feign(Class<?> feign){
        this.feign = feign;
        return this;
    }

    public AnnotationSettingBuilder method(String method){

        this.method = method;
        return this;
    }

    public AnnotationSettingBuilder args(Class<?>[] args){
        this.args = args;
        return this;
    }

    public AnnotationSettingBuilder batch(boolean batch){
        this.batch = batch;
        return this;
    }

    public AnnotationSettingBuilder enableCache(boolean enableCache){
        this.enableCache = enableCache;
        return this;
    }


    public AnnotationSettingBuilder resultType(Class<?> resultType){
        this.resultType = resultType;
        return this;
    }

    public AnnotationSettingBuilder fieldQuerySetting(String[] fieldQuerySetting){
        this.fieldQuerySetting = fieldQuerySetting;
        return this;
    }


    public AnnotationSetting build(){
        return new AnnotationSetting(key,feign,method,args,enableCache,resultType,batch);
    }

    public ClassAnnotationSetting buildClassAnnotation(final List<ClassAnnotationProperties> fieldSetting, AnnotationSetting annotationSetting){
        if (fieldSetting.size() == 0){
            if (logger.isDebugEnabled()){
                logger.debug("field is not have PolymericValue annotation");
            }
        }
        return new ClassAnnotationSetting(fieldSetting,annotationSetting);
    }
}
