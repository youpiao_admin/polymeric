package com.cdg.polymeric;


import com.cdg.polymeric.config.AnnotationSetting;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 用户收集配置聚合的vo
 */
@FunctionalInterface
public interface Collectors {

     void collect(CopyOnWriteArrayList<AnnotationSetting> annotationSettings);


}
