package com.cdg.polymeric.config;

import com.cdg.polymeric.Environment;
import com.cdg.polymeric.core.ext.excutor.AbstactFeignTemplateExcutor;
import com.google.common.cache.LoadingCache;
import org.apache.ibatis.reflection.Reflector;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * PolymericValue 的配置信息
 * @author wyp
 * @version 2021-07-16
 * {@link com.cdg.polymeric.annonation.PolymericValue}
 */
public class ClassAnnotationProperties {

    protected Environment environment;
    protected String name;
    protected boolean enableCache;
    protected LoadingCache<String, Object> caches;

    public ClassAnnotationProperties(String name){
       this(name,false);
    }

    public ClassAnnotationProperties(String name,boolean enableCache){
        this.name = name;
        this.enableCache = enableCache;
    }

    public Environment getEnvironment(){
        return this.environment;
    }

    public String getName() {
        return name;
    }

    public boolean isEnableCache() {
        return enableCache;
    }

    public void setEnvironment(Environment environment){
        this.environment = environment;
    }

    // 判断自己是否被缓存
    public boolean isCachedSelf(){
            Reflector reflector = this.environment.getReflector();
            String key = reflector.getType().getName() + this.name;
            caches = this.environment.loadCache();
            Object data = caches.getIfPresent(key);
            if (null == data){
                return false;
            }else {
                return true;
            }
        }



}
