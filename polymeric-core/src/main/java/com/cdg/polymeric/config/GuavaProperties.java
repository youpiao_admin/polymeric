package com.cdg.polymeric.config;


import lombok.Data;

/**
 * @author wyp
 * guava 配置
 * @date 2020-09-29
 */
@Data
public class GuavaProperties {


    public GuavaProperties() {

    }

    /**
     * guava缓存的键值数
     */
    private Integer guavaCacheNumMaxSize;
    /**
     * guava更新混存的下一次时间,分钟
     */
    private Integer guavaCacheRefreshWriteTime;
    /**
     * guava
     */
    private Integer guavaCacheRefreshThreadPoolSize;




    public Integer getGuavaCacheNumMaxSize() {
        return guavaCacheNumMaxSize;
    }

    public void setGuavaCacheNumMaxSize(Integer guavaCacheNumMaxSize) {
        this.guavaCacheNumMaxSize = guavaCacheNumMaxSize;
    }

    public Integer getGuavaCacheRefreshWriteTime() {
        return guavaCacheRefreshWriteTime;
    }

    public void setGuavaCacheRefreshWriteTime(Integer guavaCacheRefreshWriteTime) {
        this.guavaCacheRefreshWriteTime = guavaCacheRefreshWriteTime;
    }


}
