package com.cdg.polymeric.config;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ClassAnnotationSetting extends AnnotationSetting{

    private List<ClassAnnotationProperties> fieldSetting;

    private List<ClassAnnotationProperties> fieldCacheSetting = Collections.synchronizedList(new ArrayList<>());




    public ClassAnnotationSetting(String key, Class<?> feign, String method, Class<?>[] args, boolean enableCache, Class<?> resultType,boolean batch) {
        super(key, feign, method, args, enableCache, resultType,batch);

    }

    public ClassAnnotationSetting(List<ClassAnnotationProperties> fieldSetting,AnnotationSetting parent){
        this(parent.getKey(),parent.getFeign(),parent.getMethod(),parent.getArgs(),true,parent.getResultType(),parent.getBatch());
        this.fieldSetting = fieldSetting;

    }

    public List<ClassAnnotationProperties> getFieldSetting() {
        return fieldSetting;
    }

    /**
     * 获取已经配置了 缓存 的 ClassAnnotationProperties 集合
     * @return
     */
    public List<ClassAnnotationProperties> getFieldCacheSetting() {
        if (this.fieldSetting.size() != 0){
            for (ClassAnnotationProperties classAnnotationProperties : this.fieldSetting){
                if (classAnnotationProperties.isEnableCache()){
                    fieldCacheSetting.add(classAnnotationProperties);
                }
            }
        }

        return fieldCacheSetting;
    }
}
