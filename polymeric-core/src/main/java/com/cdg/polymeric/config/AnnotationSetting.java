package com.cdg.polymeric.config;

import com.cdg.polymeric.Environment;
import com.cdg.polymeric.Setting;

import java.util.Optional;

/**
 * 数据聚合配置
 */
public class AnnotationSetting implements Setting {

    /**
     * 查询的 key
     */
    private String key;
    /**
     * 远程 feign 接口
     */
    private Class<?> feign;
    /**
     * 远程 方法
     */
    private String method;
    /**
     * 参数
     */
    private Class<?>[] args;

    /**
     * 是否支持批量查询
     */
    private boolean batch;
    /**
     * 是否开启缓存
     */
    private boolean enableCache;
    /**
     * 聚合字段类型
     */
    private Class<?> resultType;
    /**
     * 远程返回的类型
     */
    private Class<?> targetType;

    /**
     * 字段名称
     */
    private String fieldName;

    private Environment environment;




    /**
     * 构造基本条件
     * @param key
     * @param feign
     * @param method
     * @param args
     */
    public AnnotationSetting(String key,Class<?> feign,String method,Class<?>[] args,boolean batch){
        this.key = key;
        this.feign = feign;
        this.method = method;
        this.args = args;
        this.batch = batch;
    }


    /**
     * 开启缓存
     * @param feign
     * @param method
     * @param args
     * @param enableCache
     */


    public AnnotationSetting(String key,Class<?> feign,String method,Class<?>[] args,boolean enableCache,Class<?> resultType,boolean batch){
        this.key = key;
        this.feign = feign;
        this.method = method;
        this.args = args;
        this.enableCache = enableCache;
        this.resultType = resultType;
        this.batch = batch;
    }




    public String getKey() {
        return key;
    }

    public Class<?> getFeign() {
        return feign;
    }

    public String getMethod() {
        return method;
    }

    public Class<?>[] getArgs() {
        return args;
    }

    public boolean isEnableCache() {
        return enableCache;
    }

    public Class<?> getResultType() {
        return resultType;
    }

    public Class<?> getTargetType() {
        return targetType;
    }

    public void setTargetType(Class<?> targetType) {
        this.targetType = targetType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;

    }

    @Override
    public Environment getEnvironment() {
        return this.environment;
    }

    public boolean getBatch(){
        return this.batch;
    }
}
