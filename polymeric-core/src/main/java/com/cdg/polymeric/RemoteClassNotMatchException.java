package com.cdg.polymeric;

public class RemoteClassNotMatchException extends RuntimeException{

    public RemoteClassNotMatchException(String message){
        super(message);
    }

    public RemoteClassNotMatchException(String message,Throwable e){
        super(message,e);
    }


}
