package com.cdg.polymeric;

import com.cdg.polymeric.config.AnnotationSetting;
import com.cdg.polymeric.core.ext.excutor.AbstactFeignTemplateExcutor;
import com.google.common.cache.LoadingCache;
import org.apache.ibatis.reflection.Reflector;

public  class DefaultEnvironment implements Environment {

    private Excutor excutor;
    private AnnotationSetting annotationSetting;
    private Reflector reflector;

    public DefaultEnvironment(Excutor excutor, AnnotationSetting annotationSetting,Reflector reflector){
        this.excutor = excutor;
        this.annotationSetting = annotationSetting;
        this.reflector = reflector;
    }

    @Override
    public Excutor getExcutor() {
        return this.excutor;
    }

    @Override
    public AnnotationSetting getAnnotationSetting() {
        return this.annotationSetting;
    }

    @Override
    public Reflector getReflector() {
        return this.reflector;
    }

    @Override
    public LoadingCache<String, Object> loadCache() {
        return AbstactFeignTemplateExcutor.caches;
    }
}
