package com.cdg.polymeric;

/**
 * 全局 setting 接口
 */
public interface Setting {

    void setEnvironment(Environment environment);

    Environment getEnvironment();
}
